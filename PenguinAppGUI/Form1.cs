﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Numerics;
using MathNet.Numerics;
using MathNet.Numerics.IntegralTransforms;

namespace PenguinAppGUI
{
    public partial class MainForm : Form
    {
        List<double> clock = new List<double>();
        List<double> flap = new List<double>();
        List<double> feather = new List<double>();
        List<double> pitch = new List<double>();
        List<double> alpha = new List<double>();

        List<double> clockResult = new List<double>();
        List<double> flapResult = new List<double>();
        List<double> featherResult = new List<double>();
        List<double> pitchResult = new List<double>();
        List<double> alphaResult = new List<double>();
        List<double> fxResult = new List<double>();
        List<double> fyResult = new List<double>();

        IniFile ini;
        SerialPortProcessor myPort;
        RobotControlMPC robotControl;
        Simulation simulation;
        DAQControl daqControl;
        WaveForn waveForm;

        internal SerialPortProcessor MyPort { get => myPort; set => myPort = value; }


        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            robotControl = new RobotControlMPC(MyPort);

            daqControl = new DAQControl();
            waveForm = new WaveForn();
            simulation = new Simulation();

            ini = new IniFile("./Parameter.ini");

            flapAmp.Value = decimal.Parse(ini["Flap", "Amplitude"]);
            flapFunc.SelectedIndex = flapFunc.FindString(ini["Flap", "Function"]);
            flapFreq.Value = decimal.Parse(ini["Flap", "Frequency"]);
            flapDelay.Value = decimal.Parse(ini["Flap", "Delay"]);
            flapOffset.Value = decimal.Parse(ini["Flap", "Offset"]);

            flapBar.Value = (int)flapOffset.Value;

            featherAmp.Value = decimal.Parse(ini["Feather", "Amplitude"]);
            featherFunc.SelectedIndex = featherFunc.FindString(ini["Feather", "Function"]);
            featherFreq.Value = decimal.Parse(ini["Feather", "Frequency"]);
            featherDelay.Value = decimal.Parse(ini["Feather", "Delay"]);
            featherOffset.Value = decimal.Parse(ini["Feather", "Offset"]);

            featherBar.Value = (int)featherOffset.Value;

            upAoA.Value = decimal.Parse(ini["Feather", "UpAoA"]);
            downAoA.Value = decimal.Parse(ini["Feather", "DownAoA"]);

            pitchAmp.Value = decimal.Parse(ini["Pitch", "Amplitude"]);
            pitchFunc.SelectedIndex = pitchFunc.FindString(ini["Pitch", "Function"]);
            pitchFreq.Value = decimal.Parse(ini["Pitch", "Frequency"]);
            pitchDelay.Value = decimal.Parse(ini["Pitch", "Delay"]);
            pitchOffset.Value = decimal.Parse(ini["Pitch", "Offset"]);


            pitchBar.Value = (int)pitchOffset.Value;


            velocity.Value = decimal.Parse(ini["Others", "Velocity"]);
            position.Value = decimal.Parse(ini["Others", "Position"]);
            time.Value = decimal.Parse(ini["Others", "Time"]);
            motionRate.Value = decimal.Parse(ini["Others", "Rate"]);
            
            fxOffset.Value = decimal.Parse(ini["Analyze", "FxOffset"]);
            fyOffset.Value = decimal.Parse(ini["Analyze", "FyOffset"]);
            flapSensorOffset.Value = decimal.Parse(ini["Analyze", "FlapOffset"]);
            featherSensorOffset.Value = decimal.Parse(ini["Analyze", "FeatherOffset"]);
            pitchSensorOffset.Value = decimal.Parse(ini["Analyze", "PitchOffset"]);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ini["Flap", "Amplitude"] = flapAmp.Value.ToString();
            ini["Flap", "Function"] = (String)flapFunc.SelectedItem;
            ini["Flap", "Frequency"] = flapFreq.Value.ToString();
            ini["Flap", "Delay"] = flapDelay.Value.ToString();
            ini["Flap", "Offset"] = flapOffset.Value.ToString();

            ini["Feather", "Amplitude"] = featherAmp.Value.ToString();
            ini["Feather", "Function"] = (String)featherFunc.SelectedItem;
            ini["Feather", "Frequency"] = featherFreq.Value.ToString();
            ini["Feather", "Delay"] = featherDelay.Value.ToString();
            ini["Feather", "Offset"] = featherOffset.Value.ToString();

            ini["Feather", "UpAoA"] = upAoA.Value.ToString();
            ini["Feather", "DownAoA"] = downAoA.Value.ToString();

            ini["Pitch", "Amplitude"] = pitchAmp.Value.ToString();
            ini["Pitch", "Function"] = (String)pitchFunc.SelectedItem;
            ini["Pitch", "Frequency"] = pitchFreq.Value.ToString();
            ini["Pitch", "Delay"] = pitchDelay.Value.ToString();
            ini["Pitch", "Offset"] = pitchOffset.Value.ToString();

            ini["Others", "Velocity"] = velocity.Value.ToString();
            ini["Others", "Position"] = position.Value.ToString();
            ini["Others", "Time"] = time.Value.ToString();
            ini["Others", "Rate"] = motionRate.Value.ToString();

            ini["Analyze", "FxOffset"] = fxOffset.Value.ToString();
            ini["Analyze", "FyOffset"] = fyOffset.Value.ToString();
            ini["Analyze", "FlapOffset"] = flapSensorOffset.Value.ToString();
            ini["Analyze", "FeatherOffset"] = featherSensorOffset.Value.ToString();
            ini["Analyze", "PitchOffset"] = pitchSensorOffset.Value.ToString();
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            double delta_t = 1.0 / (double)motionRate.Value;

            double t = 0;
            Func<double, double>[] func_array = { waveForm.Sin, waveForm.Triangle, waveForm.Square, waveForm.Sawtooth, waveForm.SigmoidWave };
            double flap_temp, feather_temp, pitch_temp, alpha_temp;

            double v_b = (double)velocity.Value;
            double pos = (double)position.Value;
            double a_up_ideal = (double)upAoA.Value;
            double a_down_ideal = (double)downAoA.Value;

            clock.Clear();
            flap.Clear();
            feather.Clear();
            pitch.Clear();
            alpha.Clear();

            while (t < (double)time.Value)
            {
                flap_temp = (double)flapAmp.Value * func_array[flapFunc.SelectedIndex](2 * Math.PI * (double)flapFreq.Value * t + robotControl.deg2Rad((double)flapDelay.Value)) + (double)flapOffset.Value;
                feather_temp = (double)featherAmp.Value * func_array[featherFunc.SelectedIndex](2 * Math.PI * (double)featherFreq.Value * t + robotControl.deg2Rad((double)featherDelay.Value)) + (double)featherOffset.Value;
                pitch_temp = (double)pitchAmp.Value * func_array[pitchFunc.SelectedIndex](2 * Math.PI * (double)pitchFreq.Value * t + robotControl.deg2Rad((double)pitchDelay.Value)) + (double)pitchOffset.Value;

                clock.Add(t);
                flap.Add(flap_temp);
                feather.Add(feather_temp);
                pitch.Add(pitch_temp);

                t += delta_t;
            }

            for(int i = 0; i < clock.Count; i++)
            {
                double dFlap, dFeather, dPitch;
                if (i < 1)
                {
                    dFlap = 0;
                    dFeather = 0;
                    dPitch = 0;
                }
                else
                {
                    dFlap = (flap[i] - flap[i - 1]) / (clock[i] - clock[i - 1]);
                    dFeather = (feather[i] - feather[i - 1]) / (clock[i] - clock[i - 1]);
                    dPitch = (pitch[i] - pitch[i - 1]) / (clock[i] - clock[i - 1]);
                }

                if (ctrlCheck.Checked)
                {
                    if (i < 2)
                    {
                        dFeather = 0;
                    }
                    else
                    {
                        dFeather = (feather[i - 1] - feather[i - 2]) / (clock[i - 1] - clock[i - 2]); // dummy
                    }
                    double target_aoa = 0;
                    target_aoa = (a_up_ideal - a_down_ideal) / 2 * waveForm.SigmoidWave(2 * Math.PI * (double)flapFreq.Value * clock[i] + Math.PI / 2, 10) + (a_up_ideal + a_down_ideal) / 2;
                    feather_temp = simulation.ctrlAoA(robotControl.deg2Rad(pitch[i]), robotControl.deg2Rad(flap[i]), robotControl.deg2Rad(0),
                            robotControl.deg2Rad(dPitch), robotControl.deg2Rad(dFlap), robotControl.deg2Rad(dFeather), robotControl.deg2Rad(target_aoa), pos, v_b);
                    feather_temp = robotControl.rad2Deg(feather_temp);
                    feather[i] = feather_temp;

                    if (i < 1)
                    {
                        dFeather = 0;
                    }
                    else
                    {
                        dFeather = (feather[i] - feather[i - 1]) / (clock[i] - clock[i - 1]);
                    }
                }
                alpha_temp = simulation.calculateAoA(robotControl.deg2Rad(pitch[i]), robotControl.deg2Rad(flap[i]), robotControl.deg2Rad(feather[i]), robotControl.deg2Rad(dPitch), robotControl.deg2Rad(dFlap), robotControl.deg2Rad(dFeather), pos, v_b);
                alpha_temp = robotControl.rad2Deg(alpha_temp);
                alpha.Add(alpha_temp);
            }
            
            String date_label = DateTime.Now.ToString("yyyyMMddHHmmss");
            String file_name = "A" + flapAmp.Value.ToString() + featherAmp.Value.ToString() + pitchAmp.Value.ToString() + 
                "f" + flapFreq.Value.ToString() + 
                "O" + flapOffset.Value.ToString() + featherOffset.Value.ToString() + pitchOffset.Value.ToString() +
                "V" + velocity.Value.ToString() + ctrlCheck.Checked.ToString();
            fileLabel.Text = file_name;

            exportFile(fileLabel.Text + ".csv");
            Console.WriteLine("Finish writing");
            status.Text = "Finish writing";
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileLabel.Text = openFileDialog.FileName;
                importFile(fileLabel.Text);
                Console.WriteLine("Finish reading");
                status.Text = "Finish reading";
            }
        }

        private void runButton_Click(object sender, EventArgs e)
        {        
            double wait_s = (double)wait.Value;

            if (clock.Count == 0)
            {
                MessageBox.Show("Set csv file!!");
                return;
            }

            // wait for stable
            status.Text = "Waiting";
            robotControl.setPosition(0, 0, 0);
            robotControl.wait(wait_s);
            
            // wait for stable
            status.Text = "Waiting";
            robotControl.setPosition(flap[1], feather[1], pitch[1]);
            robotControl.wait(wait_s);
            
            // motion
            status.Text = "Now runing";
            if(sensorCheck.Checked)
            {
                daqControl.start();
            }

            robotControl.setMotion(clock, flap, feather, pitch);

            // wait for stable
            status.Text = "Waiting";
            robotControl.wait(wait_s);

            robotControl.setPosition(0, 0, 0);
            robotControl.wait(wait_s);

            if (sensorCheck.Checked)
            {
                daqControl.stop();
                List<double> list = new List<double>(daqControl.getData(0));
            }

            Console.WriteLine("Finish run");
            status.Text = "Finish runing";
        }

        private void flapBar_Scroll(object sender, EventArgs e)
        {
            flapOffset.Value = flapBar.Value;
            robotControl.setPosition((double)flapOffset.Value, (double)featherOffset.Value, (double)pitchOffset.Value);

            Console.Write(" flap_deg= "); Console.WriteLine(flapOffset.Value);
        }

        private void featherBar_Scroll(object sender, EventArgs e)
        {
            featherOffset.Value = featherBar.Value;
            robotControl.setPosition((double)flapOffset.Value, (double)featherOffset.Value, (double)pitchOffset.Value);

            Console.Write(" feather_deg= "); Console.WriteLine(featherOffset.Value);
        }

        private void pitchBar_Scroll(object sender, EventArgs e)
        {
            pitchOffset.Value = pitchBar.Value;
            robotControl.setPosition((double)flapOffset.Value, (double)featherOffset.Value, (double)pitchOffset.Value);

            Console.Write(" pitch_deg= "); Console.WriteLine(pitchOffset.Value);
        }

        private void importFile(String fileName)
        {
            StreamReader reader = new StreamReader(fileName);
            clock.Clear();
            flap.Clear();
            feather.Clear();
            pitch.Clear();
            alpha.Clear();

            String list_tag = reader.ReadLine();
            var value_tag = list_tag.Split(',');

            if (value_tag.Length < 4)
            {
                MessageBox.Show("Error File Format !!");
                return;
            }

            while (!reader.EndOfStream)
            {
                String list = reader.ReadLine();
                var value = list.Split(',');
                double t = double.Parse(value[0]);
                clock.Add(t);

                double flap_temp = double.Parse(value[1]);
                flap.Add(flap_temp);
                double feather_temp = double.Parse(value[2]);
                feather.Add(feather_temp);
                double pitch_temp = double.Parse(value[3]);
                pitch.Add(pitch_temp);

                if (value_tag.Length > 4)
                {
                    double alpha_temp = double.Parse(value[4]);
                    alpha.Add(alpha_temp);
                }
            }
            reader.Close();
        }

        private void exportFile(String fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
            {

                writer.WriteLine("time" + "," + "flap" + "," + "feather" + "," + "pitch" + "," + "alpha");

                for (int i = 0; i < clock.Count; i++)
                {
                    writer.WriteLine(clock[i] + "," + flap[i] + "," + feather[i] + "," + pitch[i] + "," + alpha[i]);
                }

                writer.Close();
            }
        }

        private void sensorCheck_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox check = (CheckBox)sender;
            if(check.Checked)
            {
                List<String> sensorArray = new List<String> { ini["Sensor", "Fx"], ini["Sensor", "Fy"], ini["Sensor", "Flap"], ini["Sensor", "Feather"], ini["Sensor", "Pitch"] };
                int rate = (int)samplingRate.Value;
                daqControl.initDAQ(sensorArray, -10, 10, rate, fileLabel.Text + "ResultRaw");
            }
        }

        private void analyzeButton_Click(object sender, EventArgs e)
        {
            double alpha_temp;
            double v_b = (double)velocity.Value;
            double pos = (double)position.Value;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileLabel.Text = openFileDialog.FileName;
                importResultFile(fileLabel.Text);
                Console.WriteLine("Finish reading");
                status.Text = "Finish reading";

                // filter
                
                applyFilter(fxResult);
                applyFilter(fyResult);
                applyFilter(flapResult);
                applyFilter(featherResult);
                applyFilter(pitchResult);
                
                for (int i = 0; i < fxResult.Count; i++)
                {
                    double dFlap, dFeather, dPitch;
                    if (i < 1)
                    {
                        dFlap = 0;
                        dFeather = 0;
                        dPitch = 0;
                    }
                    else
                    {
                        dFlap = (flapResult[i] - flapResult[i - 1]) / (clockResult[i] - clockResult[i - 1]);
                        dFeather = (featherResult[i] - featherResult[i - 1]) / (clockResult[i] - clockResult[i - 1]);
                        dPitch = (pitchResult[i] - pitchResult[i - 1]) / (clockResult[i] - clockResult[i - 1]);
                    }

                    alpha_temp = simulation.calculateAoA(robotControl.deg2Rad(pitchResult[i]), robotControl.deg2Rad(flapResult[i]), robotControl.deg2Rad(featherResult[i]), robotControl.deg2Rad(dPitch), robotControl.deg2Rad(dFlap), robotControl.deg2Rad(dFeather), pos, v_b);
                    alpha_temp = robotControl.rad2Deg(alpha_temp);
                    alphaResult.Add(alpha_temp);
                }
                exportResultFile(fileLabel.Text + "Analyze.csv");
                Console.WriteLine("Finish writing");
                status.Text = "Finish writing";
            }
        }

        private void importResultFile(String fileName)
        {
            StreamReader reader = new StreamReader(fileName);
            clockResult.Clear();
            flapResult.Clear();
            featherResult.Clear();
            pitchResult.Clear();
            alphaResult.Clear();
            fxResult.Clear();
            fyResult.Clear();

            String list_tag = reader.ReadLine();
            var value_tag = list_tag.Split(',');

            if (value_tag.Length < 4)
            {
                MessageBox.Show("Error File Format !!");
                return;
            }

            int index = 0;
            double rate = (double)samplingRate.Value;

            while (!reader.EndOfStream)
            {
                String list = reader.ReadLine();
                var value = list.Split(',');
                double t = double.Parse(value[0]);
                clockResult.Add(t);

                //double fx = double.Parse(value[1]) - (double)fxOffset.Value;
                double fx = double.Parse(value[1]);
                fxResult.Add(fx);
                //double fy = double.Parse(value[2]) - (double)fyOffset.Value;
                double fy = double.Parse(value[2]);
                fyResult.Add(fy);

                //double flap_temp = (double)slope.Value * (double.Parse(value[3]) - (double)flapSensorOffset.Value) / 5 * 0.5 - (double)flapOffset.Value;
                double flap_temp = double.Parse(value[3]);
                flapResult.Add(flap_temp);

                //double feather_temp = (double)slope.Value * (double.Parse(value[4]) - (double)featherSensorOffset.Value) / 5 * -1 - (double)featherOffset.Value;
                double feather_temp = double.Parse(value[4]);
                featherResult.Add(feather_temp);

                //double pitch_temp =  (double)slope.Value * (double.Parse(value[5]) - (double)pitchSensorOffset.Value) / 5 * -1 - (double)pitchOffset.Value;
                double pitch_temp = double.Parse(value[5]);
                pitchResult.Add(pitch_temp);

                index++;
            }
            reader.Close();
        }

        private void exportResultFile(String fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
            {

                writer.WriteLine("time" + "," + "Fx" + "," + "Fy" + "," + "flap" + "," + "feather" + "," + "pitch" + "," + "alpha");

                for (int i = 0; i < fxResult.Count; i++)
                {
                    writer.WriteLine(clockResult[i] + "," + fxResult[i] + "," + fyResult[i] + "," + flapResult[i] + "," + featherResult[i] + "," + pitchResult[i] + "," + alphaResult[i]);
                }

                writer.Close();
            }
        }

        private void applyFilter(List<double> value)
        {
            int length = 1;
            while (length < value.Count)
            {
                length *= 2;
            }
            length /= 2;

            Complex[] complexValue = new Complex[length];
            //convert int to complex
            for (int i = 0; i < length; i++)
            {
                complexValue[i] = new Complex(value[i], 0);   //　
            }
            value.Clear();
            //run FFT
            Fourier.Forward(complexValue, FourierOptions.Default); // arbitrary length

            int cut_index = (int)(length * cutRate.Value / samplingRate.Value);
            for (int i = cut_index; i < length - cut_index; i++)
            {
                complexValue[i] = 0;
            }

            Fourier.Inverse(complexValue);

            for (int i = 0; i < length; i++)
            {
                value.Add(complexValue[i].Real);
            }
        }

        private void detectOffset(List<double> value)
        {
            double edge = 0;
            double value_ave = 0, value_sum = 0;
            int index;
            for (index = 0; index < value.Count; index++)
            {
                edge = value[index] - value_ave;
                if (edge > 0.01 && index > 1)
                {
                    break;
                }

                // renew average
                value_sum += value[index];
                value_ave = value_sum / index;
            }

            for (int i = 0; i < value.Count; i++)
            {
                value[i] = value[i + index] - value_ave;
            }
        }

        public void DataReceivedCallback(byte[] data)
        {
            Invoke(new MethodInvoker(() => Console.Write(System.Text.Encoding.ASCII.GetString(data))));
        }
    }
}
