﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using NationalInstruments;
using NationalInstruments.DAQmx;


namespace PenguinAppGUI
{
    class DAQControl
    {
        private int samplingNumber;

        private AnalogMultiChannelReader analogInReader;
        private NationalInstruments.DAQmx.Task myTask;
        private NationalInstruments.DAQmx.Task runningTask;
        private AsyncCallback analogCallback;
        private List<double>[] logQue;

        public DAQControl()
        {

        }

        public void initDAQ(List<String> channelList, double minVoltage, double maxVoltage, int samplingRate, String fileName)
        {
            try
            {
                // Create a new task
                myTask = new NationalInstruments.DAQmx.Task();

                logQue = new List<double>[channelList.Count];

                // Create virtual channels
                for(int index = 0; index < channelList.Count; index++)
                {
                    myTask.AIChannels.CreateVoltageChannel(channelList[index], "", (AITerminalConfiguration)(-1), minVoltage, maxVoltage, AIVoltageUnits.Volts);
                    logQue[index] = new List<double>();
                }

                // Configure the timing parameters
                samplingNumber = 1000;
                myTask.Timing.ConfigureSampleClock("", samplingRate,
                SampleClockActiveEdge.Rising, SampleQuantityMode.ContinuousSamples, samplingNumber);


                // Configure TDMS Logging
                if (fileName.Trim().Length > 0)
                    myTask.ConfigureLogging(fileName, TdmsLoggingOperation.CreateOrReplace, LoggingMode.LogAndRead, "Group Name");

                // Verify the Task
                myTask.Control(TaskAction.Verify);
            }
            catch (DaqException exception)
            {
                // Display Errors
                runningTask = null;
                myTask.Dispose();
            }
        }

        public void start()
        {
            runningTask = myTask;
            analogInReader = new AnalogMultiChannelReader(myTask.Stream);
            analogCallback = new AsyncCallback(AnalogInCallback);

            // Use SynchronizeCallbacks to specify that the object
            // marshals callbacks across threads appropriately.
            analogInReader.SynchronizeCallbacks = true;
            analogInReader.BeginReadWaveform(samplingNumber, analogCallback, myTask);
        }

        public void stop()
        {
            myTask.Dispose();
            runningTask = null;
        }

        public List<double> getData(int channel)
        {
            return logQue[channel];
        }

        private void AnalogInCallback(IAsyncResult ar)
        {
            try
            {
                if (runningTask != null && runningTask == ar.AsyncState)
                {
                    // Read the available data from the channels
                    AnalogWaveform<double>[] data = analogInReader.EndReadWaveform(ar);

                    // Plot your data here
                    int channel = 0;
                    foreach (AnalogWaveform<double> form in data)
                    {
                        for(int i = 0; i < form.SampleCount; i++)
                        {
                            logQue[channel].Add(form.Samples[i].Value);
                        }
                        channel++;
                    }

                    analogInReader.BeginMemoryOptimizedReadWaveform(samplingNumber, analogCallback, myTask, data);
                }
            }
            catch (DaqException exception)
            {
                // Display Errors
                runningTask = null;
                myTask.Dispose();
            }
        }
    }
}
