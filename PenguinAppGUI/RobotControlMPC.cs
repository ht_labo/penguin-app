﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenguinAppGUI
{
    class RobotControlMPC
    {
        // data structure
        const int CMD = 0;
        const int SC = 1;
        const int POS_H = 1;
        const int POS_L = 2;
        // commnad header
        const int POSITION_CMD = 0b10000000;
        const int READ_CMD = 0b10100000;
        const int WRITE_CMD = 0b11000000;
        const int ID_CMD = 0b11100000;
        // sub command
        const int EEPROM_SC = 0x00;
        const int STRC_SC = 0x01;
        const int SPD_SC = 0x02;
        const int CUR_SC = 0x03;
        const int TMP_SC = 0x04;

        // communicate protocol
        const int START_BYTE = 0xFE;
        const int END_BYTE = 0xFF;

        SerialPortProcessor myPort;

        public RobotControlMPC(SerialPortProcessor port)
        {
            myPort = port;
        }

        // convert 180 degree to 12 bit value
        private int deg2Data(double degree)
        {
            if(degree > 90)
            {
                degree = 90;
            }else if(degree < -90)
            {
                degree = -90;
            }
            return (int)((4095 / 180.0) * (degree - 90.0) + 4095);
        }

        private double data2Deg(int data)
        {
            if (data > 4095)
            {
                data = 4095;
            }
            else if (data < 0)
            {
                data = 0;
            }
            return (180.0 / 4095) * (data - 4095) + 90;
        }

        public double deg2Rad(double degree)
        {
            return degree / 180.0 * Math.PI;
        }

        public double rad2Deg(double rad)
        {
            return rad / Math.PI * 180.0;
        }

        public void setPosition(double flap, double feather, double pitch)
        {
            int flap_temp, feather_temp, pitch_temp;
            byte[] tx = new byte[8];

            // degree to signal
            flap_temp = deg2Data(flap);
            feather_temp = deg2Data(feather);
            pitch_temp = deg2Data(pitch);

            tx[0] = START_BYTE;

            tx[1] = (byte)((flap_temp >> 7) & 0x7F);
            tx[2] = (byte)(flap_temp & 0x7F);

            tx[3] = (byte)((feather_temp >> 7) & 0x7F);
            tx[4] = (byte)(feather_temp & 0x7F);

            tx[5] = (byte)((pitch_temp >> 7) & 0x7F);
            tx[6] = (byte)(pitch_temp & 0x7F);

            tx[7] = END_BYTE;

            myPort.WriteData(tx);

            for(int i = 0; i< tx.Length; i++)
            {
                Console.Write((int)tx[i]);
                Console.Write(" ");
            }
            Console.WriteLine();
            //myPort.DiscardInBuffer();
        }

        public void setMotion(List<double> clock, List<double> flap, List<double> feather, List<double> pitch)
        {
            double t, t_start;
            int number = clock.Count();
            int index = 1;
            double flap_temp, feather_temp, pitch_temp;

            t = 0;
            t_start = System.DateTime.Now.Ticks / 10000.0;
            while (t < clock[number - 1])
            {
                t = System.DateTime.Now.Ticks / 10000.0 - t_start;
                t = t / 1000.0; // convert ms to s

                for (; index < number; index++)
                {
                    if (clock[index - 1] <= t && t < clock[index])
                    {
                        flap_temp = map(t, clock[index - 1], clock[index], flap[index - 1], flap[index]);
                        feather_temp = map(t, clock[index - 1], clock[index], feather[index - 1], feather[index]);
                        pitch_temp = map(t, clock[index - 1], clock[index], pitch[index - 1], pitch[index]);

                        setPosition(flap_temp, feather_temp, pitch_temp);
                        for (int i = 0; i < 100000; i++) ;
                        break;
                    }
                }
            }
        }

        public void wait(double seconds)
        {
            double t, t_start;
            t = 0;
            t_start = System.DateTime.Now.Ticks / 10000.0;
            while (t < seconds)
            {
                t = System.DateTime.Now.Ticks / 10000.0 - t_start;
                t = t / 1000.0; // convert ms to s
            }
        }

        private double map(double x, double x0, double x1, double y0, double y1)
        {
            return (y1 - y0) / (x1 - x0) * (x - x0) + y0;
        }
    }
}
