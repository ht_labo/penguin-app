﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenguinAppGUI
{
    class WaveForn
    {
        public double Triangle(double t)
        {
            t = t % (2 * Math.PI);
            if (0 <= t && t < Math.PI / 2)
            {
                return 2 / Math.PI * t;
            }
            else if (Math.PI / 2 <= t && t < 3 * Math.PI / 2)
            {
                return -2 / Math.PI * (t - Math.PI / 2) + 1;
            }
            else
            {
                return 2 / Math.PI * (t - 3 * Math.PI / 2) - 1;
            }
        }

        public double Square(double t)
        {
            t = t % (2 * Math.PI);
            if (0 <= t && t < Math.PI)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public double Sawtooth(double t)
        {
            t = t % (2 * Math.PI);
            return 0.5 / Math.PI * t;
        }

        public double Sin(double t)
        {
            return Math.Sin(t);
        }

        private double Sigmoid(double x, double a)
        {
            return 2 * (1 / (1 + Math.Exp(-a * x))) - 1;
        }

        public double SigmoidWave(double t, double a)
        {
            t = t % (2 * Math.PI);
            if (0 <= t && t < Math.PI / 2)
            {
                return Sigmoid(t, a);
            }
            else if (Math.PI / 2 <= t && t < 3 * Math.PI / 2)
            {
                return -Sigmoid(t - Math.PI, a);
            }
            else
            {
                return Sigmoid(t - 2 * Math.PI, a);
            }
        }
        public double SigmoidWave(double t)
        {
            double a = 25;
            t = t % (2 * Math.PI);
            if (0 <= t && t < Math.PI / 2)
            {
                return Sigmoid(t, a);
            }
            else if (Math.PI / 2 <= t && t < 3 * Math.PI / 2)
            {
                return -Sigmoid(t - Math.PI, a);
            }
            else
            {
                return Sigmoid(t - 2 * Math.PI, a);
            }
        }
    }
}
