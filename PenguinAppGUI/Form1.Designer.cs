﻿namespace PenguinAppGUI
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.flapLabel = new System.Windows.Forms.TextBox();
            this.featherLabel = new System.Windows.Forms.TextBox();
            this.pitchLabel = new System.Windows.Forms.TextBox();
            this.amplitudeLabel = new System.Windows.Forms.TextBox();
            this.functionLabel = new System.Windows.Forms.TextBox();
            this.frequencyLabel = new System.Windows.Forms.TextBox();
            this.delayLabel = new System.Windows.Forms.TextBox();
            this.flapAmp = new System.Windows.Forms.NumericUpDown();
            this.featherAmp = new System.Windows.Forms.NumericUpDown();
            this.pitchAmp = new System.Windows.Forms.NumericUpDown();
            this.flapFunc = new System.Windows.Forms.ComboBox();
            this.featherFunc = new System.Windows.Forms.ComboBox();
            this.pitchFunc = new System.Windows.Forms.ComboBox();
            this.flapFreq = new System.Windows.Forms.NumericUpDown();
            this.featherFreq = new System.Windows.Forms.NumericUpDown();
            this.pitchFreq = new System.Windows.Forms.NumericUpDown();
            this.flapDelay = new System.Windows.Forms.NumericUpDown();
            this.featherDelay = new System.Windows.Forms.NumericUpDown();
            this.pitchDelay = new System.Windows.Forms.NumericUpDown();
            this.generateButton = new System.Windows.Forms.Button();
            this.velocityLabel = new System.Windows.Forms.TextBox();
            this.positionLabel = new System.Windows.Forms.TextBox();
            this.upAoALabel = new System.Windows.Forms.TextBox();
            this.ctrlCheck = new System.Windows.Forms.CheckBox();
            this.velocity = new System.Windows.Forms.NumericUpDown();
            this.position = new System.Windows.Forms.NumericUpDown();
            this.upAoA = new System.Windows.Forms.NumericUpDown();
            this.runButton = new System.Windows.Forms.Button();
            this.timeLable = new System.Windows.Forms.TextBox();
            this.time = new System.Windows.Forms.NumericUpDown();
            this.motionRate = new System.Windows.Forms.NumericUpDown();
            this.openButton = new System.Windows.Forms.Button();
            this.fileLabel = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.flapBar = new System.Windows.Forms.TrackBar();
            this.featherBar = new System.Windows.Forms.TrackBar();
            this.pitchBar = new System.Windows.Forms.TrackBar();
            this.offsetLabel = new System.Windows.Forms.TextBox();
            this.featherOffset = new System.Windows.Forms.NumericUpDown();
            this.flapOffset = new System.Windows.Forms.NumericUpDown();
            this.pitchOffset = new System.Windows.Forms.NumericUpDown();
            this.downAoALabel = new System.Windows.Forms.TextBox();
            this.downAoA = new System.Windows.Forms.NumericUpDown();
            this.status = new System.Windows.Forms.TextBox();
            this.statusLabel = new System.Windows.Forms.TextBox();
            this.sensorCheck = new System.Windows.Forms.CheckBox();
            this.waitLabel = new System.Windows.Forms.TextBox();
            this.wait = new System.Windows.Forms.NumericUpDown();
            this.motionRateLabel = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.samplingRate = new System.Windows.Forms.NumericUpDown();
            this.analyzeButton = new System.Windows.Forms.Button();
            this.fxOffsetLabel = new System.Windows.Forms.TextBox();
            this.fxOffset = new System.Windows.Forms.NumericUpDown();
            this.fyOffset = new System.Windows.Forms.NumericUpDown();
            this.fyOffsetLabel = new System.Windows.Forms.TextBox();
            this.featherSensorOffset = new System.Windows.Forms.NumericUpDown();
            this.featherOffsetLabel = new System.Windows.Forms.TextBox();
            this.flapSensorOffset = new System.Windows.Forms.NumericUpDown();
            this.flapOffsetLabel = new System.Windows.Forms.TextBox();
            this.pitchSensorOffset = new System.Windows.Forms.NumericUpDown();
            this.pitchOffsetLabel = new System.Windows.Forms.TextBox();
            this.slopeLabel = new System.Windows.Forms.TextBox();
            this.cutRate = new System.Windows.Forms.NumericUpDown();
            this.cutRateLabel = new System.Windows.Forms.TextBox();
            this.slope = new System.Windows.Forms.NumericUpDown();
            this.filterCheck = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.flapAmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherAmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchAmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.velocity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.position)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upAoA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motionRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downAoA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samplingRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fxOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fyOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherSensorOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapSensorOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchSensorOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cutRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slope)).BeginInit();
            this.SuspendLayout();
            // 
            // flapLabel
            // 
            this.flapLabel.Location = new System.Drawing.Point(38, 93);
            this.flapLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flapLabel.Name = "flapLabel";
            this.flapLabel.Size = new System.Drawing.Size(74, 22);
            this.flapLabel.TabIndex = 0;
            this.flapLabel.Text = "Flap";
            // 
            // featherLabel
            // 
            this.featherLabel.Location = new System.Drawing.Point(38, 187);
            this.featherLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.featherLabel.Name = "featherLabel";
            this.featherLabel.Size = new System.Drawing.Size(74, 22);
            this.featherLabel.TabIndex = 1;
            this.featherLabel.Text = "Feather";
            // 
            // pitchLabel
            // 
            this.pitchLabel.Location = new System.Drawing.Point(38, 280);
            this.pitchLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pitchLabel.Name = "pitchLabel";
            this.pitchLabel.Size = new System.Drawing.Size(74, 22);
            this.pitchLabel.TabIndex = 2;
            this.pitchLabel.Text = "Pitch";
            // 
            // amplitudeLabel
            // 
            this.amplitudeLabel.Location = new System.Drawing.Point(154, 27);
            this.amplitudeLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.amplitudeLabel.Name = "amplitudeLabel";
            this.amplitudeLabel.Size = new System.Drawing.Size(74, 22);
            this.amplitudeLabel.TabIndex = 3;
            this.amplitudeLabel.Text = "Amplitude";
            // 
            // functionLabel
            // 
            this.functionLabel.Location = new System.Drawing.Point(269, 27);
            this.functionLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.functionLabel.Name = "functionLabel";
            this.functionLabel.Size = new System.Drawing.Size(74, 22);
            this.functionLabel.TabIndex = 4;
            this.functionLabel.Text = "Function";
            // 
            // frequencyLabel
            // 
            this.frequencyLabel.Location = new System.Drawing.Point(384, 27);
            this.frequencyLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.frequencyLabel.Name = "frequencyLabel";
            this.frequencyLabel.Size = new System.Drawing.Size(74, 22);
            this.frequencyLabel.TabIndex = 5;
            this.frequencyLabel.Text = "Frequency";
            // 
            // delayLabel
            // 
            this.delayLabel.Location = new System.Drawing.Point(499, 27);
            this.delayLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.delayLabel.Name = "delayLabel";
            this.delayLabel.Size = new System.Drawing.Size(74, 22);
            this.delayLabel.TabIndex = 6;
            this.delayLabel.Text = "Delay";
            // 
            // flapAmp
            // 
            this.flapAmp.Location = new System.Drawing.Point(154, 93);
            this.flapAmp.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flapAmp.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.flapAmp.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.flapAmp.Name = "flapAmp";
            this.flapAmp.Size = new System.Drawing.Size(74, 22);
            this.flapAmp.TabIndex = 7;
            // 
            // featherAmp
            // 
            this.featherAmp.Location = new System.Drawing.Point(154, 187);
            this.featherAmp.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.featherAmp.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.featherAmp.Name = "featherAmp";
            this.featherAmp.Size = new System.Drawing.Size(74, 22);
            this.featherAmp.TabIndex = 8;
            // 
            // pitchAmp
            // 
            this.pitchAmp.Location = new System.Drawing.Point(154, 280);
            this.pitchAmp.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pitchAmp.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.pitchAmp.Name = "pitchAmp";
            this.pitchAmp.Size = new System.Drawing.Size(74, 22);
            this.pitchAmp.TabIndex = 9;
            // 
            // flapFunc
            // 
            this.flapFunc.FormattingEnabled = true;
            this.flapFunc.Items.AddRange(new object[] {
            "Sin",
            "Triangle",
            "Square",
            "Sawtooth",
            "Sigmoid"});
            this.flapFunc.Location = new System.Drawing.Point(269, 93);
            this.flapFunc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flapFunc.Name = "flapFunc";
            this.flapFunc.Size = new System.Drawing.Size(74, 23);
            this.flapFunc.TabIndex = 10;
            // 
            // featherFunc
            // 
            this.featherFunc.FormattingEnabled = true;
            this.featherFunc.Items.AddRange(new object[] {
            "Sin",
            "Triangle",
            "Square",
            "Sawtooth",
            "Sigmoid"});
            this.featherFunc.Location = new System.Drawing.Point(269, 187);
            this.featherFunc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.featherFunc.Name = "featherFunc";
            this.featherFunc.Size = new System.Drawing.Size(74, 23);
            this.featherFunc.TabIndex = 11;
            // 
            // pitchFunc
            // 
            this.pitchFunc.FormattingEnabled = true;
            this.pitchFunc.Items.AddRange(new object[] {
            "Sin",
            "Triangle",
            "Square",
            "Sawtooth",
            "Sigmoid"});
            this.pitchFunc.Location = new System.Drawing.Point(269, 280);
            this.pitchFunc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pitchFunc.Name = "pitchFunc";
            this.pitchFunc.Size = new System.Drawing.Size(74, 23);
            this.pitchFunc.TabIndex = 12;
            // 
            // flapFreq
            // 
            this.flapFreq.DecimalPlaces = 1;
            this.flapFreq.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.flapFreq.Location = new System.Drawing.Point(384, 93);
            this.flapFreq.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flapFreq.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.flapFreq.Name = "flapFreq";
            this.flapFreq.Size = new System.Drawing.Size(74, 22);
            this.flapFreq.TabIndex = 13;
            // 
            // featherFreq
            // 
            this.featherFreq.DecimalPlaces = 1;
            this.featherFreq.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.featherFreq.Location = new System.Drawing.Point(384, 187);
            this.featherFreq.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.featherFreq.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.featherFreq.Name = "featherFreq";
            this.featherFreq.Size = new System.Drawing.Size(74, 22);
            this.featherFreq.TabIndex = 14;
            // 
            // pitchFreq
            // 
            this.pitchFreq.DecimalPlaces = 1;
            this.pitchFreq.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.pitchFreq.Location = new System.Drawing.Point(384, 280);
            this.pitchFreq.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pitchFreq.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.pitchFreq.Name = "pitchFreq";
            this.pitchFreq.Size = new System.Drawing.Size(74, 22);
            this.pitchFreq.TabIndex = 15;
            // 
            // flapDelay
            // 
            this.flapDelay.Location = new System.Drawing.Point(499, 93);
            this.flapDelay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flapDelay.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.flapDelay.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.flapDelay.Name = "flapDelay";
            this.flapDelay.Size = new System.Drawing.Size(74, 22);
            this.flapDelay.TabIndex = 16;
            // 
            // featherDelay
            // 
            this.featherDelay.Location = new System.Drawing.Point(499, 187);
            this.featherDelay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.featherDelay.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.featherDelay.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.featherDelay.Name = "featherDelay";
            this.featherDelay.Size = new System.Drawing.Size(74, 22);
            this.featherDelay.TabIndex = 17;
            // 
            // pitchDelay
            // 
            this.pitchDelay.Location = new System.Drawing.Point(499, 280);
            this.pitchDelay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pitchDelay.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.pitchDelay.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.pitchDelay.Name = "pitchDelay";
            this.pitchDelay.Size = new System.Drawing.Size(74, 22);
            this.pitchDelay.TabIndex = 18;
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(384, 533);
            this.generateButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(102, 28);
            this.generateButton.TabIndex = 19;
            this.generateButton.Text = "Generate";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // velocityLabel
            // 
            this.velocityLabel.Location = new System.Drawing.Point(38, 373);
            this.velocityLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.velocityLabel.Name = "velocityLabel";
            this.velocityLabel.Size = new System.Drawing.Size(74, 22);
            this.velocityLabel.TabIndex = 20;
            this.velocityLabel.Text = "Velocity";
            // 
            // positionLabel
            // 
            this.positionLabel.Location = new System.Drawing.Point(38, 427);
            this.positionLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.positionLabel.Name = "positionLabel";
            this.positionLabel.Size = new System.Drawing.Size(74, 22);
            this.positionLabel.TabIndex = 21;
            this.positionLabel.Text = "Position";
            // 
            // upAoALabel
            // 
            this.upAoALabel.Location = new System.Drawing.Point(384, 373);
            this.upAoALabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.upAoALabel.Name = "upAoALabel";
            this.upAoALabel.Size = new System.Drawing.Size(74, 22);
            this.upAoALabel.TabIndex = 22;
            this.upAoALabel.Text = "Up AoA";
            // 
            // ctrlCheck
            // 
            this.ctrlCheck.AutoSize = true;
            this.ctrlCheck.Location = new System.Drawing.Point(269, 373);
            this.ctrlCheck.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ctrlCheck.Name = "ctrlCheck";
            this.ctrlCheck.Size = new System.Drawing.Size(107, 19);
            this.ctrlCheck.TabIndex = 23;
            this.ctrlCheck.Text = "Control AoA";
            this.ctrlCheck.UseVisualStyleBackColor = true;
            // 
            // velocity
            // 
            this.velocity.DecimalPlaces = 2;
            this.velocity.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.velocity.Location = new System.Drawing.Point(154, 373);
            this.velocity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.velocity.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.velocity.Name = "velocity";
            this.velocity.Size = new System.Drawing.Size(74, 22);
            this.velocity.TabIndex = 24;
            // 
            // position
            // 
            this.position.DecimalPlaces = 2;
            this.position.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.position.Location = new System.Drawing.Point(154, 427);
            this.position.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.position.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.position.Name = "position";
            this.position.Size = new System.Drawing.Size(74, 22);
            this.position.TabIndex = 25;
            // 
            // upAoA
            // 
            this.upAoA.Location = new System.Drawing.Point(499, 373);
            this.upAoA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.upAoA.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.upAoA.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.upAoA.Name = "upAoA";
            this.upAoA.Size = new System.Drawing.Size(74, 22);
            this.upAoA.TabIndex = 26;
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(499, 533);
            this.runButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(100, 83);
            this.runButton.TabIndex = 27;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // timeLable
            // 
            this.timeLable.Location = new System.Drawing.Point(38, 480);
            this.timeLable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.timeLable.Name = "timeLable";
            this.timeLable.Size = new System.Drawing.Size(132, 22);
            this.timeLable.TabIndex = 28;
            this.timeLable.Text = "Motion Time [s]";
            // 
            // time
            // 
            this.time.DecimalPlaces = 1;
            this.time.Location = new System.Drawing.Point(269, 480);
            this.time.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.time.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(74, 22);
            this.time.TabIndex = 29;
            // 
            // motionRate
            // 
            this.motionRate.Location = new System.Drawing.Point(269, 533);
            this.motionRate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.motionRate.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.motionRate.Name = "motionRate";
            this.motionRate.Size = new System.Drawing.Size(74, 22);
            this.motionRate.TabIndex = 31;
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(384, 587);
            this.openButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(102, 27);
            this.openButton.TabIndex = 33;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // fileLabel
            // 
            this.fileLabel.Location = new System.Drawing.Point(38, 640);
            this.fileLabel.Margin = new System.Windows.Forms.Padding(2);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(449, 22);
            this.fileLabel.TabIndex = 34;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // flapBar
            // 
            this.flapBar.Location = new System.Drawing.Point(154, 120);
            this.flapBar.Margin = new System.Windows.Forms.Padding(2);
            this.flapBar.Maximum = 90;
            this.flapBar.Minimum = -90;
            this.flapBar.Name = "flapBar";
            this.flapBar.Size = new System.Drawing.Size(422, 56);
            this.flapBar.TabIndex = 35;
            this.flapBar.Scroll += new System.EventHandler(this.flapBar_Scroll);
            // 
            // featherBar
            // 
            this.featherBar.Location = new System.Drawing.Point(154, 213);
            this.featherBar.Margin = new System.Windows.Forms.Padding(2);
            this.featherBar.Maximum = 90;
            this.featherBar.Minimum = -90;
            this.featherBar.Name = "featherBar";
            this.featherBar.Size = new System.Drawing.Size(422, 56);
            this.featherBar.TabIndex = 36;
            this.featherBar.Scroll += new System.EventHandler(this.featherBar_Scroll);
            // 
            // pitchBar
            // 
            this.pitchBar.Location = new System.Drawing.Point(154, 307);
            this.pitchBar.Margin = new System.Windows.Forms.Padding(2);
            this.pitchBar.Maximum = 90;
            this.pitchBar.Minimum = -90;
            this.pitchBar.Name = "pitchBar";
            this.pitchBar.Size = new System.Drawing.Size(422, 56);
            this.pitchBar.TabIndex = 37;
            this.pitchBar.Scroll += new System.EventHandler(this.pitchBar_Scroll);
            // 
            // offsetLabel
            // 
            this.offsetLabel.Location = new System.Drawing.Point(614, 27);
            this.offsetLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.offsetLabel.Name = "offsetLabel";
            this.offsetLabel.Size = new System.Drawing.Size(74, 22);
            this.offsetLabel.TabIndex = 38;
            this.offsetLabel.Text = "Offset";
            // 
            // featherOffset
            // 
            this.featherOffset.Location = new System.Drawing.Point(614, 187);
            this.featherOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.featherOffset.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.featherOffset.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.featherOffset.Name = "featherOffset";
            this.featherOffset.Size = new System.Drawing.Size(74, 22);
            this.featherOffset.TabIndex = 40;
            // 
            // flapOffset
            // 
            this.flapOffset.Location = new System.Drawing.Point(614, 93);
            this.flapOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flapOffset.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.flapOffset.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.flapOffset.Name = "flapOffset";
            this.flapOffset.Size = new System.Drawing.Size(74, 22);
            this.flapOffset.TabIndex = 41;
            // 
            // pitchOffset
            // 
            this.pitchOffset.Location = new System.Drawing.Point(614, 280);
            this.pitchOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pitchOffset.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.pitchOffset.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.pitchOffset.Name = "pitchOffset";
            this.pitchOffset.Size = new System.Drawing.Size(74, 22);
            this.pitchOffset.TabIndex = 42;
            // 
            // downAoALabel
            // 
            this.downAoALabel.Location = new System.Drawing.Point(384, 427);
            this.downAoALabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.downAoALabel.Name = "downAoALabel";
            this.downAoALabel.Size = new System.Drawing.Size(83, 22);
            this.downAoALabel.TabIndex = 43;
            this.downAoALabel.Text = "Down AoA";
            // 
            // downAoA
            // 
            this.downAoA.Location = new System.Drawing.Point(499, 427);
            this.downAoA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.downAoA.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.downAoA.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.downAoA.Name = "downAoA";
            this.downAoA.Size = new System.Drawing.Size(74, 22);
            this.downAoA.TabIndex = 44;
            // 
            // status
            // 
            this.status.Location = new System.Drawing.Point(499, 480);
            this.status.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(180, 22);
            this.status.TabIndex = 46;
            // 
            // statusLabel
            // 
            this.statusLabel.Location = new System.Drawing.Point(384, 480);
            this.statusLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(83, 22);
            this.statusLabel.TabIndex = 45;
            this.statusLabel.Text = "Status";
            // 
            // sensorCheck
            // 
            this.sensorCheck.AutoSize = true;
            this.sensorCheck.Location = new System.Drawing.Point(38, 693);
            this.sensorCheck.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.sensorCheck.Name = "sensorCheck";
            this.sensorCheck.Size = new System.Drawing.Size(120, 19);
            this.sensorCheck.TabIndex = 47;
            this.sensorCheck.Text = "Sensor Enable";
            this.sensorCheck.UseVisualStyleBackColor = true;
            this.sensorCheck.CheckedChanged += new System.EventHandler(this.sensorCheck_CheckedChanged);
            // 
            // waitLabel
            // 
            this.waitLabel.Location = new System.Drawing.Point(38, 587);
            this.waitLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.waitLabel.Name = "waitLabel";
            this.waitLabel.Size = new System.Drawing.Size(129, 22);
            this.waitLabel.TabIndex = 49;
            this.waitLabel.Text = "Wait Time [s]";
            // 
            // wait
            // 
            this.wait.Location = new System.Drawing.Point(269, 587);
            this.wait.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.wait.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.wait.Name = "wait";
            this.wait.Size = new System.Drawing.Size(74, 22);
            this.wait.TabIndex = 50;
            this.wait.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // motionRateLabel
            // 
            this.motionRateLabel.Location = new System.Drawing.Point(38, 533);
            this.motionRateLabel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.motionRateLabel.Name = "motionRateLabel";
            this.motionRateLabel.Size = new System.Drawing.Size(129, 22);
            this.motionRateLabel.TabIndex = 30;
            this.motionRateLabel.Text = "Motion Rate [Hz]";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(845, 427);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(129, 22);
            this.textBox1.TabIndex = 51;
            this.textBox1.Text = "Sampling Rate [Hz]";
            // 
            // samplingRate
            // 
            this.samplingRate.Location = new System.Drawing.Point(1050, 427);
            this.samplingRate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.samplingRate.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.samplingRate.Name = "samplingRate";
            this.samplingRate.Size = new System.Drawing.Size(74, 22);
            this.samplingRate.TabIndex = 52;
            this.samplingRate.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // analyzeButton
            // 
            this.analyzeButton.Location = new System.Drawing.Point(1168, 32);
            this.analyzeButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.analyzeButton.Name = "analyzeButton";
            this.analyzeButton.Size = new System.Drawing.Size(100, 83);
            this.analyzeButton.TabIndex = 53;
            this.analyzeButton.Text = "Analyze";
            this.analyzeButton.UseVisualStyleBackColor = true;
            this.analyzeButton.Click += new System.EventHandler(this.analyzeButton_Click);
            // 
            // fxOffsetLabel
            // 
            this.fxOffsetLabel.Location = new System.Drawing.Point(845, 27);
            this.fxOffsetLabel.Margin = new System.Windows.Forms.Padding(2);
            this.fxOffsetLabel.Name = "fxOffsetLabel";
            this.fxOffsetLabel.Size = new System.Drawing.Size(81, 22);
            this.fxOffsetLabel.TabIndex = 54;
            this.fxOffsetLabel.Text = "Fx Offset";
            // 
            // fxOffset
            // 
            this.fxOffset.DecimalPlaces = 16;
            this.fxOffset.Location = new System.Drawing.Point(1050, 27);
            this.fxOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.fxOffset.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.fxOffset.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.fxOffset.Name = "fxOffset";
            this.fxOffset.Size = new System.Drawing.Size(74, 22);
            this.fxOffset.TabIndex = 55;
            // 
            // fyOffset
            // 
            this.fyOffset.DecimalPlaces = 16;
            this.fyOffset.Location = new System.Drawing.Point(1050, 93);
            this.fyOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.fyOffset.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.fyOffset.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.fyOffset.Name = "fyOffset";
            this.fyOffset.Size = new System.Drawing.Size(74, 22);
            this.fyOffset.TabIndex = 57;
            // 
            // fyOffsetLabel
            // 
            this.fyOffsetLabel.Location = new System.Drawing.Point(845, 93);
            this.fyOffsetLabel.Margin = new System.Windows.Forms.Padding(2);
            this.fyOffsetLabel.Name = "fyOffsetLabel";
            this.fyOffsetLabel.Size = new System.Drawing.Size(81, 22);
            this.fyOffsetLabel.TabIndex = 56;
            this.fyOffsetLabel.Text = "Fx Offset";
            // 
            // featherSensorOffset
            // 
            this.featherSensorOffset.DecimalPlaces = 16;
            this.featherSensorOffset.Location = new System.Drawing.Point(1050, 227);
            this.featherSensorOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.featherSensorOffset.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.featherSensorOffset.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.featherSensorOffset.Name = "featherSensorOffset";
            this.featherSensorOffset.Size = new System.Drawing.Size(74, 22);
            this.featherSensorOffset.TabIndex = 61;
            // 
            // featherOffsetLabel
            // 
            this.featherOffsetLabel.Location = new System.Drawing.Point(845, 227);
            this.featherOffsetLabel.Margin = new System.Windows.Forms.Padding(2);
            this.featherOffsetLabel.Name = "featherOffsetLabel";
            this.featherOffsetLabel.Size = new System.Drawing.Size(81, 22);
            this.featherOffsetLabel.TabIndex = 60;
            this.featherOffsetLabel.Text = "Feather Offset";
            // 
            // flapSensorOffset
            // 
            this.flapSensorOffset.DecimalPlaces = 16;
            this.flapSensorOffset.Location = new System.Drawing.Point(1050, 160);
            this.flapSensorOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flapSensorOffset.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.flapSensorOffset.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.flapSensorOffset.Name = "flapSensorOffset";
            this.flapSensorOffset.Size = new System.Drawing.Size(74, 22);
            this.flapSensorOffset.TabIndex = 59;
            // 
            // flapOffsetLabel
            // 
            this.flapOffsetLabel.Location = new System.Drawing.Point(845, 160);
            this.flapOffsetLabel.Margin = new System.Windows.Forms.Padding(2);
            this.flapOffsetLabel.Name = "flapOffsetLabel";
            this.flapOffsetLabel.Size = new System.Drawing.Size(81, 22);
            this.flapOffsetLabel.TabIndex = 58;
            this.flapOffsetLabel.Text = "Flap Offset";
            // 
            // pitchSensorOffset
            // 
            this.pitchSensorOffset.DecimalPlaces = 16;
            this.pitchSensorOffset.Location = new System.Drawing.Point(1050, 293);
            this.pitchSensorOffset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pitchSensorOffset.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.pitchSensorOffset.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.pitchSensorOffset.Name = "pitchSensorOffset";
            this.pitchSensorOffset.Size = new System.Drawing.Size(74, 22);
            this.pitchSensorOffset.TabIndex = 63;
            // 
            // pitchOffsetLabel
            // 
            this.pitchOffsetLabel.Location = new System.Drawing.Point(845, 293);
            this.pitchOffsetLabel.Margin = new System.Windows.Forms.Padding(2);
            this.pitchOffsetLabel.Name = "pitchOffsetLabel";
            this.pitchOffsetLabel.Size = new System.Drawing.Size(81, 22);
            this.pitchOffsetLabel.TabIndex = 62;
            this.pitchOffsetLabel.Text = "Pitch Offset";
            // 
            // slopeLabel
            // 
            this.slopeLabel.Location = new System.Drawing.Point(845, 360);
            this.slopeLabel.Margin = new System.Windows.Forms.Padding(2);
            this.slopeLabel.Name = "slopeLabel";
            this.slopeLabel.Size = new System.Drawing.Size(103, 22);
            this.slopeLabel.TabIndex = 64;
            this.slopeLabel.Text = "Slope [deg./V]";
            // 
            // cutRate
            // 
            this.cutRate.Location = new System.Drawing.Point(1050, 560);
            this.cutRate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cutRate.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.cutRate.Name = "cutRate";
            this.cutRate.Size = new System.Drawing.Size(74, 22);
            this.cutRate.TabIndex = 65;
            this.cutRate.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // cutRateLabel
            // 
            this.cutRateLabel.Location = new System.Drawing.Point(845, 560);
            this.cutRateLabel.Margin = new System.Windows.Forms.Padding(2);
            this.cutRateLabel.Name = "cutRateLabel";
            this.cutRateLabel.Size = new System.Drawing.Size(154, 22);
            this.cutRateLabel.TabIndex = 66;
            this.cutRateLabel.Text = "Cut Off Frequency [Hz}";
            // 
            // slope
            // 
            this.slope.DecimalPlaces = 16;
            this.slope.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.slope.Location = new System.Drawing.Point(1050, 360);
            this.slope.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.slope.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.slope.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.slope.Name = "slope";
            this.slope.Size = new System.Drawing.Size(90, 22);
            this.slope.TabIndex = 67;
            this.slope.Value = new decimal(new int[] {
            251104,
            0,
            0,
            -2147287040});
            // 
            // filterCheck
            // 
            this.filterCheck.AutoSize = true;
            this.filterCheck.Location = new System.Drawing.Point(845, 493);
            this.filterCheck.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.filterCheck.Name = "filterCheck";
            this.filterCheck.Size = new System.Drawing.Size(107, 19);
            this.filterCheck.TabIndex = 68;
            this.filterCheck.Text = "Filter Enable";
            this.filterCheck.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1539, 822);
            this.Controls.Add(this.filterCheck);
            this.Controls.Add(this.slope);
            this.Controls.Add(this.cutRateLabel);
            this.Controls.Add(this.cutRate);
            this.Controls.Add(this.slopeLabel);
            this.Controls.Add(this.pitchSensorOffset);
            this.Controls.Add(this.pitchOffsetLabel);
            this.Controls.Add(this.featherSensorOffset);
            this.Controls.Add(this.featherOffsetLabel);
            this.Controls.Add(this.flapSensorOffset);
            this.Controls.Add(this.flapOffsetLabel);
            this.Controls.Add(this.fyOffset);
            this.Controls.Add(this.fyOffsetLabel);
            this.Controls.Add(this.fxOffset);
            this.Controls.Add(this.fxOffsetLabel);
            this.Controls.Add(this.analyzeButton);
            this.Controls.Add(this.samplingRate);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.wait);
            this.Controls.Add(this.waitLabel);
            this.Controls.Add(this.sensorCheck);
            this.Controls.Add(this.status);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.downAoA);
            this.Controls.Add(this.downAoALabel);
            this.Controls.Add(this.pitchOffset);
            this.Controls.Add(this.flapOffset);
            this.Controls.Add(this.featherOffset);
            this.Controls.Add(this.offsetLabel);
            this.Controls.Add(this.pitchBar);
            this.Controls.Add(this.featherBar);
            this.Controls.Add(this.flapBar);
            this.Controls.Add(this.fileLabel);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.motionRate);
            this.Controls.Add(this.motionRateLabel);
            this.Controls.Add(this.time);
            this.Controls.Add(this.timeLable);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.upAoA);
            this.Controls.Add(this.position);
            this.Controls.Add(this.velocity);
            this.Controls.Add(this.ctrlCheck);
            this.Controls.Add(this.upAoALabel);
            this.Controls.Add(this.positionLabel);
            this.Controls.Add(this.velocityLabel);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.pitchDelay);
            this.Controls.Add(this.featherDelay);
            this.Controls.Add(this.flapDelay);
            this.Controls.Add(this.pitchFreq);
            this.Controls.Add(this.featherFreq);
            this.Controls.Add(this.flapFreq);
            this.Controls.Add(this.pitchFunc);
            this.Controls.Add(this.featherFunc);
            this.Controls.Add(this.flapFunc);
            this.Controls.Add(this.pitchAmp);
            this.Controls.Add(this.featherAmp);
            this.Controls.Add(this.flapAmp);
            this.Controls.Add(this.delayLabel);
            this.Controls.Add(this.frequencyLabel);
            this.Controls.Add(this.functionLabel);
            this.Controls.Add(this.amplitudeLabel);
            this.Controls.Add(this.pitchLabel);
            this.Controls.Add(this.featherLabel);
            this.Controls.Add(this.flapLabel);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainForm";
            this.Text = "MainMenu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flapAmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherAmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchAmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.velocity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.position)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upAoA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motionRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downAoA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samplingRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fxOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fyOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featherSensorOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flapSensorOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pitchSensorOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cutRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slope)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox flapLabel;
        private System.Windows.Forms.TextBox featherLabel;
        private System.Windows.Forms.TextBox pitchLabel;
        private System.Windows.Forms.TextBox amplitudeLabel;
        private System.Windows.Forms.TextBox functionLabel;
        private System.Windows.Forms.TextBox frequencyLabel;
        private System.Windows.Forms.TextBox delayLabel;
        private System.Windows.Forms.NumericUpDown flapAmp;
        private System.Windows.Forms.NumericUpDown featherAmp;
        private System.Windows.Forms.NumericUpDown pitchAmp;
        private System.Windows.Forms.ComboBox flapFunc;
        private System.Windows.Forms.ComboBox featherFunc;
        private System.Windows.Forms.ComboBox pitchFunc;
        private System.Windows.Forms.NumericUpDown flapFreq;
        private System.Windows.Forms.NumericUpDown featherFreq;
        private System.Windows.Forms.NumericUpDown pitchFreq;
        private System.Windows.Forms.NumericUpDown flapDelay;
        private System.Windows.Forms.NumericUpDown featherDelay;
        private System.Windows.Forms.NumericUpDown pitchDelay;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.TextBox velocityLabel;
        private System.Windows.Forms.TextBox positionLabel;
        private System.Windows.Forms.TextBox upAoALabel;
        private System.Windows.Forms.CheckBox ctrlCheck;
        private System.Windows.Forms.NumericUpDown velocity;
        private System.Windows.Forms.NumericUpDown position;
        private System.Windows.Forms.NumericUpDown upAoA;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.TextBox timeLable;
        private System.Windows.Forms.NumericUpDown time;
        private System.Windows.Forms.NumericUpDown motionRate;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.TextBox fileLabel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TrackBar flapBar;
        private System.Windows.Forms.TrackBar featherBar;
        private System.Windows.Forms.TrackBar pitchBar;
        private System.Windows.Forms.TextBox offsetLabel;
        private System.Windows.Forms.NumericUpDown featherOffset;
        private System.Windows.Forms.NumericUpDown flapOffset;
        private System.Windows.Forms.NumericUpDown pitchOffset;
        private System.Windows.Forms.TextBox downAoALabel;
        private System.Windows.Forms.NumericUpDown downAoA;
        private System.Windows.Forms.TextBox status;
        private System.Windows.Forms.TextBox statusLabel;
        private System.Windows.Forms.CheckBox sensorCheck;
        private System.Windows.Forms.TextBox waitLabel;
        private System.Windows.Forms.NumericUpDown wait;
        private System.Windows.Forms.TextBox motionRateLabel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown samplingRate;
        private System.Windows.Forms.Button analyzeButton;
        private System.Windows.Forms.TextBox fxOffsetLabel;
        private System.Windows.Forms.NumericUpDown fxOffset;
        private System.Windows.Forms.NumericUpDown fyOffset;
        private System.Windows.Forms.TextBox fyOffsetLabel;
        private System.Windows.Forms.NumericUpDown featherSensorOffset;
        private System.Windows.Forms.TextBox featherOffsetLabel;
        private System.Windows.Forms.NumericUpDown flapSensorOffset;
        private System.Windows.Forms.TextBox flapOffsetLabel;
        private System.Windows.Forms.NumericUpDown pitchSensorOffset;
        private System.Windows.Forms.TextBox pitchOffsetLabel;
        private System.Windows.Forms.TextBox slopeLabel;
        private System.Windows.Forms.NumericUpDown cutRate;
        private System.Windows.Forms.TextBox cutRateLabel;
        private System.Windows.Forms.NumericUpDown slope;
        private System.Windows.Forms.CheckBox filterCheck;
    }
}

