﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenguinAppGUI
{
    class RobotControl
    {
        // data structure
        const int CMD = 0;
        const int SC = 1;
        const int POS_H = 1;
        const int POS_L = 2;
        // commnad header
        const int POSITION_CMD = 0b10000000;
        const int READ_CMD = 0b10100000;
        const int WRITE_CMD = 0b11000000;
        const int ID_CMD = 0b11100000;
        // sub command
        const int EEPROM_SC = 0x00;
        const int STRC_SC = 0x01;
        const int SPD_SC = 0x02;
        const int CUR_SC = 0x03;
        const int TMP_SC = 0x04;

        SerialPortProcessor myPort;

        public RobotControl(SerialPortProcessor port)
        {
            myPort = port;
        }


        private int setMotorPosition(int id, int pos)
        {
            byte[] rx, tx;

            rx = new byte[256];
            tx = new byte[3];

            tx[CMD] = (byte)(POSITION_CMD | id);
            if (pos < 3500)
            {
                pos = 3500;
            }
            else if (pos > 11500)
            {
                pos = 11500;
            }
            tx[POS_H] = (byte)((pos >> 7) & 0x7F);
            tx[POS_L] = (byte)(pos & 0x7F);

            myPort.WriteData(tx);

            myPort.DiscardInBuffer();
            return (rx[1] << 7) + rx[2];
        }

        private int deg2Pos(int degree)
        {
            return (int)((11500 - 3500) / (270.0) * (degree - 135.0) + 11500);
        }

        public double deg2Rad(double degree)
        {
            return degree / 180.0 * Math.PI;
        }

        public double rad2Deg(double rad)
        {
            return rad / Math.PI * 180.0;
        }

        public void setPosition(double flap, double feather, double pitch)
        {
            setMotorPosition(0, deg2Pos(-(int)(flap - feather) / 2));
            for (int i = 0; i < 100000; i++) { }
            setMotorPosition(1, deg2Pos((int)(flap + feather) / 2));
            for (int i = 0; i < 100000; i++) { }
            setMotorPosition(2, deg2Pos((int)pitch / 2));
            for (int i = 0; i < 100000; i++) { }
        }

        public void setMotion(List<double> clock, List<double> flap, List<double> feather, List<double> pitch)
        {
            double t, t_start;
            int number = clock.Count();
            int index = 1;
            double flap_temp, feather_temp, pitch_temp;

            t = 0;
            t_start = System.DateTime.Now.Ticks / 10000.0;
            while (t < clock[number - 1])
            {
                t = System.DateTime.Now.Ticks / 10000.0 - t_start;
                t = t / 1000.0; // convert ms to s

                for (; index < number; index++)
                {
                    if (clock[index - 1] <= t && t < clock[index])
                    {
                        flap_temp = map(t, clock[index - 1], clock[index], flap[index - 1], flap[index]);
                        feather_temp = map(t, clock[index - 1], clock[index], feather[index - 1], feather[index]);
                        pitch_temp = map(t, clock[index - 1], clock[index], pitch[index - 1], pitch[index]);

                        setPosition(flap_temp, feather_temp, pitch_temp);
                        break;
                    }
                }
            }
        }

        public void wait(double seconds)
        {
            double t, t_start;
            t = 0;
            t_start = System.DateTime.Now.Ticks / 10000.0;
            while (t < seconds)
            {
                t = System.DateTime.Now.Ticks / 10000.0 - t_start;
                t = t / 1000.0; // convert ms to s
            }
        }

        private double map(double x, double x0, double x1, double y0, double y1)
        {
            return (y1 - y0) / (x1 - x0) * (x - x0) + y0;
        }
    }
}
