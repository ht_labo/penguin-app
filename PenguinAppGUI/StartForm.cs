﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenguinAppGUI
{
    public partial class StartForm : Form
    {
        SerialPortProcessor myPort;

        public StartForm()
        {
            InitializeComponent();

            myPort = new SerialPortProcessor();
            string[] PortList = SerialPort.GetPortNames();
            portList.Items.Clear();
            portList.Items.AddRange(SerialPort.GetPortNames());
            if (SerialPort.GetPortNames().Length > 0)
            {
                portList.SelectedIndex = 0;
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            MainForm form = new MainForm();

            myPort.PortName = (String)portList.SelectedItem;
            /*
            myPort.BaudRate = 1250000;
            myPort.Parity = Parity.Even;
            myPort.DataBits = 8;
            myPort.StopBits = StopBits.One;
            */
            myPort.BaudRate = 9600;
            myPort.Parity = Parity.None;
            myPort.DataBits = 8;
            myPort.StopBits = StopBits.One;
            myPort.DataReceived += form.DataReceivedCallback;
            
            myPort.Open();


            form.MyPort = myPort;
            form.ShowDialog(this);
            form.Dispose();
            Dispose();
        }
    }
}
