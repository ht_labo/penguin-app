﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Numerics;
using MathNet.Numerics;
using MathNet.Numerics.IntegralTransforms;

namespace PenguinAppGUI
{
    class Analyzer
    {
        public int cutFrequency = 0;
        public int samplingRate = 0;

        private void applyFilter(List<double> value)
        {
            int length = 1;
            while (length < value.Count)
            {
                length *= 2;
            }
            length /= 2;

            Complex[] complexValue = new Complex[length];
            //convert int to complex
            for (int i = 0; i < length; i++)
            {
                complexValue[i] = new Complex(value[i], 0);   //　
            }
            value.Clear();
            //run FFT
            Fourier.Forward(complexValue, FourierOptions.Default); // arbitrary length

            int cut_index = (int)(length * cutFrequency / samplingRate);
            for (int i = cut_index; i < length - cut_index; i++)
            {
                complexValue[i] = 0;
            }

            Fourier.Inverse(complexValue);

            for (int i = 0; i < length; i++)
            {
                value.Add(complexValue[i].Real);
            }
        }

        private void detectOffset(List<double> value)
        {
            double edge = 0;
            double value_ave = 0, value_sum = 0;
            int index;
            for(index = 0; index < value.Count; index++)
            {
                edge = value[index] - value_ave;
                if(edge > 0.01 && index > 1)
                {
                    break;
                }

                // renew average
                value_sum += value[index];
                value_ave = value_sum / index;
            }

            for(int i = 0; i < value.Count; i++)
            {
                value[i] = value[i + index] - value_ave;
            }
        }
    }
}
