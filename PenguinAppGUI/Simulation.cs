﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MathNet.Numerics.LinearAlgebra.Double;

namespace PenguinAppGUI
{
    class Simulation
    {
        enum axis {X, Y, Z}
        private double wingLength = 0.125;
        private double RHO = 1000.0;

        public double ctrlAoA(double pitch, double flap, double feather, double dPitch, double dFlap, double dFeather, double a, double pos, double vbx)
        {
            double error = 10000;
            List<double> errorList = new List<double>();
            if(dFlap > 0)
            {
                feather = -Math.PI / 2;
            }
            else
            {
                feather = Math.PI / 2;
            }


            while (Math.Abs(error) > 0.001)
            {
                double a_true = calculateAoA(pitch, flap, feather, dPitch, dFlap, dFeather, pos, vbx);
                double f = a - a_true;
                double df = 0 -(calculateAoA(pitch, flap, feather, dPitch, dFlap, dFeather, pos, vbx) - calculateAoA(pitch, flap, feather - 0.0001, dPitch, dFlap, dFeather, pos, vbx)) / 0.0001;
                error = f / df;
                feather = feather - f / df;
                errorList.Add(error);
            }

            return feather;
        }

        public double calculateAoA(double pitch, double flap, double feather, double dPitch, double dFlap, double dFeather, double pos, double vbx)
        {
            DenseVector Br = DenseVector.OfArray(new double[] { 0, wingLength * pos, 0 });
            DenseVector Ov_b = DenseVector.OfArray(new double[] { vbx, 0, 0 });

            DenseVector Ov_in = -dR(pitch, flap, feather, dPitch, dFlap, dFeather) * Br - Ov_b;
            DenseVector Bv_in = (DenseVector)(R(pitch, flap, feather).Inverse() * Ov_in);

            DenseVector Bn_sxy, Bn_s, Bn_sz;

            Bn_sxy = (DenseVector)DenseVector.OfArray(new double[] { Bv_in[0], Bv_in[1], 0 }).Normalize(2);
            Bn_sz = (DenseVector)DenseVector.OfArray(new double[] { 0, 0, Bv_in[2] }).Normalize(2);

            Bn_s = CrossProduct3D(Bn_sxy, Bn_sz);

            DenseVector Bv_inh = Rodriguez(Bn_s, Math.PI / 2) * Bv_in;

            return Math.Atan2(Bv_in[2], Math.Sqrt(Bv_in[0] * Bv_in[0] + Bv_in[1] * Bv_in[1]));
        }

        private double CL(double alpha)
        {
            double degree = rad2Deg(alpha);
            // Maeda dataset
            double[,] dataSet = new double[,] { { -45, -0.61 }, { -30, -0.62 }, { -25, -0.62 }, { -20, -0.63 }, { -15, -0.62 }, { -10, -0.47 }, { -5, -0.25 }, { 0, 0.01 }, { 5, 0.26 }, { 10, 0.49 }, { 15, 0.56 }, { 20, 0.58 }, { 25, 0.55 }, { 30, 0.59 }, { 45, 0.57 } };
            // Kayasuga measured
            //double[,] dataSet = new double[,] { { -40, -0.48 }, { -35, -0.48 }, { -30, -0.44 }, { -25, -0.41 }, { -20, -0.37 }, { -15, -0.35 }, { -10, -0.37 }, { -5, -0.28 }, { 0, -0.15 }, { 5, 0.01 }, { 10, 0.40 }, { 15, 0.46 }, { 20, 0.45 }, { 25, 0.46 }, { 30, 0.48 }, { 35, 0.47 }, { 40, 0.48 } };

            for (int index = 0; index < dataSet.GetLength(0) - 1; index++)
            {
                if (dataSet[index, 0] <= degree && degree <= dataSet[index + 1, 0])
                {
                    return (dataSet[index + 1, 1] - dataSet[index, 1]) / (dataSet[index + 1, 0] - dataSet[index, 0]) * (degree - dataSet[index, 0]) + dataSet[index, 1];
                }
            }
            Console.WriteLine("Out of AoA" + degree);
            return 0.0;
        }

        private double CD(double alpha)
        {
            double degree = rad2Deg(alpha);
            // Maeda dataset
            double[,] dataSet = new double[,] { { -45, 0.49 }, { -30, 0.31 }, { -25, 0.25 }, { -20, 0.18 }, { -15, 0.12 }, { -10, 0.07 }, { -5, 0.04 }, { 0, 0.04 }, { 5, 0.04 }, { 10, 0.07 }, { 15, 0.12 }, { 20, 0.19 }, { 25, 0.25 }, { 30, 0.32 }, { 45, 0.51 } };
            // Kayasuga measured
            //double[,] dataSet = new double[,] { { -40, 0.46 }, { -35, 0.43 }, { -30, 0.36 }, { -25, 0.31 }, { -20, 0.24 }, { -15, 0.21 }, { -10, 0.16 }, { -5, 0.11 }, { 0, 0.09 }, { 5, 0.10 }, { 10, 0.15 }, { 15, 0.20 }, { 20, 0.26 }, { 25, 0.35 }, { 30, 0.41 }, { 35, 0.47 }, { 40, 0.54 } };
            for (int index = 0; index < dataSet.GetLength(0) - 1; index++)
            {
                if (dataSet[index, 0] <= degree && degree <= dataSet[index + 1, 0])
                {
                    return (dataSet[index + 1, 1] - dataSet[index, 1]) / (dataSet[index + 1, 0] - dataSet[index, 0]) * (degree - dataSet[index, 0]) + dataSet[index, 1];
                }
            }
            Console.WriteLine("Out of AoA" + degree);
            return 0.0;
        }

        private double leadEdgeArray(double r)
        {
            double rmm = r * 1000;
            double lead = 0;
            double[,] dataSet = new double[,] { { 0, 2.986 }, { 6, 7.124 }, { 12, 10.197 }, { 18, 11.862 }, { 24, 13.237 }, { 30, 14.835 }, { 36, 16.602 }, { 42, 18.148 }, { 48, 19.618 }, { 54, 20.912 }, { 60, 21.732 }, { 66, 22.672 }, { 72, 23.431 }, { 78, 23.774 }, { 84, 24.045 }, { 90, 24.125 }, { 96, 23.848 }, { 102, 23.021 }, { 108, 21.419 }, { 114, 18.906 }, { 120, 15.584 }, { 126, 11.413 } };
            for (int index = 0; index < dataSet.GetLength(0) - 1; index++)
            {
                if (dataSet[index, 0] <= rmm && rmm <= dataSet[index + 1, 0])
                {
                    lead = (dataSet[index + 1, 1] - dataSet[index, 1]) / (dataSet[index + 1, 0] - dataSet[index, 0]) * (rmm - dataSet[index, 0]) + dataSet[index, 1];
                }
            }
            return 0.001 * lead;
        }

        private double tailEdgeArray(double r)
        {
            double rmm = r * 1000;
            double tail = 0;
            double[,] dataSet = new double[,] { { 0, -3.034 }, { 6, -5.646 }, { 12, -8.505 }, { 18, -11.946 }, { 24, -15.592 }, { 30, -19.016 }, { 36, -20.192 }, { 42, -20.443 }, { 48, -19.286 }, { 54, -16.281 }, { 60, -13.729 }, { 66, -10.287 }, { 72, -8.016 }, { 78, -6.673 }, { 84, -5.126 }, { 90, -3.781 }, { 96, -2.873 }, { 102, -1.068 }, { 108, 0.283 }, { 114, 1.243 }, { 120, 2.232 }, { 126, 4.583 } };
            for (int index = 0; index < dataSet.GetLength(0) - 1; index++)
            {
                if (dataSet[index, 0] <= rmm && rmm <= dataSet[index + 1, 0])
                {
                    tail = (dataSet[index + 1, 1] - dataSet[index, 1]) / (dataSet[index + 1, 0] - dataSet[index, 0]) * (rmm - dataSet[index, 0]) + dataSet[index, 1];
                }
            }
            return 0.001 * tail;
        }

        private DenseMatrix R(double pitch, double flap, double feather)
        {
            return Rpitch(pitch) * Rflap(flap) * Rfeather(feather);
        }

        private DenseMatrix dR(double pitch, double flap, double feather, double dPitch, double dFlap, double dFeather)
        {
            return dRpitch(pitch, dPitch) * Rflap(flap) * Rfeather(feather) + Rpitch(pitch) * dRflap(flap, dFlap) * Rfeather(feather) + Rpitch(pitch) * Rflap(flap) * dRfeather(feather, dFeather);
        }

        private DenseMatrix Rpitch(double pitch)
        {
            return Ry(pitch);
        }

        private DenseMatrix Rflap(double flap)
        {
            return Rx(flap);
        }

        private DenseMatrix Rfeather(double feather)
        {
            return Ry(feather);
        }

        private DenseMatrix dRpitch(double pitch, double dPitch)
        {
            return dRy(pitch, dPitch);
        }

        private DenseMatrix dRflap(double flap, double dFlap)
        {
            return dRx(flap, dFlap);
        }

        private DenseMatrix dRfeather(double feather, double dFeather)
        {
            return dRy(feather, dFeather);
        }

        private DenseMatrix Rx(double x)
        {
            double Cx = Math.Cos(x), Sx = Math.Sin(x);
            var Rx = DenseMatrix.OfArray(new double[,] { { 1, 0, 0 },
                                                          { 0, Cx, -Sx },
                                                          { 0, Sx, Cx } });
            return Rx;
        }

        private DenseMatrix Ry(double y)
        {
            double Cy = Math.Cos(y), Sy = Math.Sin(y);
            var Ry = DenseMatrix.OfArray(new double[,] { { Cy, 0, Sy },
                                                         { 0, 1, 0 },
                                                         { -Sy, 0, Cy } });
            return Ry;
        }

        private DenseMatrix Rz(double z)
        {
            double Cz = Math.Cos(z), Sz = Math.Sin(z);
            var Rz = DenseMatrix.OfArray(new double[,] { { Cz, -Sz, 0 },
                                                         { Sz, Cz, 0 },
                                                         { 0, 0, 1 } });
            return Rz;
        }

        private DenseMatrix dRx(double x, double dx)
        {
            double Cx = Math.Cos(x), Sx = Math.Sin(x);
            var dRx = DenseMatrix.OfArray(new double[,] { { 0, 0, 0 },
                                                          { 0, -Sx * dx, -Cx * dx },
                                                          { 0, Cx * dx, -Sx * dx } });
            return dRx;
        }

        private DenseMatrix dRy(double y, double dy)
        {
            double Cy = Math.Cos(y), Sy = Math.Sin(y);
            var dRy = DenseMatrix.OfArray(new double[,] { { -Sy * dy, 0, Cy * dy },
                                                         { 0, 0, 0 },
                                                         { -Cy * dy, 0, -Sy * dy } });
            return dRy;
        }

        private DenseMatrix dRz(double z, double dz)
        {
            double Cz = Math.Cos(z), Sz = Math.Sin(z);
            var dRz = DenseMatrix.OfArray(new double[,] { { -Sz * dz, -Cz * dz, 0 },
                                                         { Cz * dz, -Sz * dz, 0 },
                                                         { 0, 0, 0 } });
            return dRz;
        }

        private DenseMatrix Rodriguez(DenseVector n, double theta)
        {
            double C = Math.Cos(theta), S = Math.Sin(theta);
            var R = DenseMatrix.OfArray(new double[,] { { C + n[0] * n[0] * (1 - C), n[0] * n[1] * (1 - C) - n[2] * S, n[2] * n[0] * (1 - C) + n[1] * S },
                                                         { n[0] * n[1] * (1 - C) + n[2] * S, C + n[1] * n[1] * (1 - C), n[1] * n[2] * (1 - C) - n[0] * S },
                                                         { n[2] * n[0] * (1 - C) -n[1] * S, n[1] * n[2] * (1 - C) + n[0] * S, C + n[2] * n[2] * (1 - C) } });
            return R;
        }

        private DenseVector CrossProduct3D(DenseVector a, DenseVector b)
        {
            DenseVector result = new DenseVector(3);
            result[0] = a[1] * b[2] - a[2] * b[1];
            result[1] = a[2] * b[0] - a[0] * b[2];
            result[2] = a[0] * b[1] - a[1] * b[0];

            return result;
        }

        private double deg2Rad(double degree)
        {
            return degree / 180.0 * Math.PI;
        }

        private double rad2Deg(double rad)
        {
            return rad / Math.PI * 180.0;
        }
    }
}
